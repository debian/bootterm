#!/bin/sh
#
# Copyright (c) 2024 Faidon Liambotis <paravoid@debian.org>
# Licensed under the same terms as BootTerm.
#
# Test output with:
#   generate-manpage.sh bin/bootterm | man -E UTF-8 -l -Tutf8 - | less -R
#
# Lint with:
#   LC_ALL=C.UTF-8 MANROFFSEQ='' MANWIDTH=80 man --warnings -E UTF-8 -l -Tutf8 -Z - >/dev/null

set -e

# add '\"t for the "tbl" preprocessor, as README.md contains tables
# Replace if/when https://bugs.debian.org/1067238 gets fixed
echo "'\\\" t"

( 
  echo '[DESCRIPTION]'
  # extract the short description in the header, "Bootterm is a ..."
  sed -n '/^# BootTerm/,/^#/ {/^#/!p}' README.md | lowdown -tman
  # extract the Main Features section, to attach to the description
  sed -n '/^## Main Features/,/^##[^#]/ {/^## /!p}' README.md | lowdown -tman

  echo '[EXAMPLES]'
  # extract the entirety of the Using it section, including subheaders
  sed -n '/^## Using it/,/^##[^#]/ {/^## /!p}' README.md | lowdown -tman

  # other misc items
  cat <<-EOF
	[REPORTING BUGS]
	Report bugs to <https://github.com/wtarreau/bootterm>

	[COPYRIGHT]
	Copyright \(co Willy Tarreau <w@1wt.eu>
	EOF
) | help2man \
	--version-option="-V" \
	--no-discard-stderr \
	--locale=C.UTF-8 \
	--section=1 \
	--name="simple terminal to ease connections with SBCs" \
	--no-info \
	--include=- \
	$@
